import sqlite3
import mysql.connector
import os
import csv
from flask import g

def get_db():
    db = getattr(g, 'db', None)
    if db is None:
        db = mysql.connector.connect(
            host='farmdb.cprt1kzl5myl.us-east-2.rds.amazonaws.com',
            user='admin',
            password='autoKar_09',
            database='farmdb'
        )
        g.db = db
    return db

def query_db(query, args=(), one=False):
    cur = get_db().cursor()
    cur.execute(query, args)
    rv = cur.fetchall() if not one else cur.fetchone()
    cur.close()
    return rv

def create_table():
    query = """
            CREATE TABLE IF NOT EXISTS animals (
                animal_id integer PRIMARY KEY,
                kind text NOT NULL,
                name text NOT NULL,
                year integer,
                sex text
            );
            """
    update_db(query)
    return "Created DB Table: Success!"
    
def create_animal(animal_id, kind, name, year, sex):
    query = ''' INSERT INTO animals(animal_id, kind, name, year, sex)
        VALUES(%s,%s,%s,%s,%s) '''
    params = (animal_id, kind, name, year, sex)
    try:
        update_db(query, params)
        return "Animal Create: Success!"
    except mysql.connector.IntegrityError as e:
        return f"Create Animal ERROR: {e}"
        
def update_db(query, args=()):
    db = get_db()
    cur = db.cursor()
    cur.execute(query, args)
    db.commit()
    cur.close()

def update_animal(animal_id, kind, name, year, sex):
    sql_query = """ UPDATE animals SET
            kind=%s,
            name=%s,
            year=%s,
            sex=%s
            WHERE animal_id=%s """
    params = (kind, name, year, sex, animal_id)
    try:
        update_db(sql_query, params)
        return "Update Animal: Success!"
    except mysql.connector.IntegrityError as e:
        return f"Update Animal ERROR: {e}"
        
def delete_animal(animal_id):
    sql = 'DELETE FROM animals WHERE animal_id=%s'
    params = (animal_id,)
    update_db(sql, params)
    return f"Deleted animal {animal_id}"

def select_all():
    sql_query = "SELECT * FROM animals"
    rows = query_db(sql_query)
    return rows

def select_by_id(animal_id):
    sql_query = "SELECT * FROM animals WHERE animal_id = %s"
    row = query_db(sql_query, (animal_id,), one=True)
    return row

def get_kinds():
    sql_query = "SELECT DISTINCT kind FROM animals"
    rows = query_db(sql_query)
    return rows

def get_names(kind):
    sql_query = "SELECT name FROM animals WHERE kind = %s"
    rows = query_db(sql_query, (kind,))
    return rows

def select(filters):
    sql_query = "SELECT * FROM animals WHERE "
    params = []
    for k, v in filters.items():
        sql_query += f"{k} = %s AND "
        params.append(v)
    sql_query = sql_query[:-5]
    rows = query_db(sql_query, tuple(params))
    return rows