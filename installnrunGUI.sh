#!/bin/bash


python3 -m venv venv
. venv/bin/activate 

export FLASK_APP=flask_app.py
export FLASK_ENV=production

pip install Flask
pip install mysql-connector-python
flask run --port 5000
flask --app flask_app.py run --debug
python3 -m flask run 