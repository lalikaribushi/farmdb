import os
import csv
import json
import mysql.connector
from flask import Flask, render_template, request, redirect, g, flash
from werkzeug.utils import secure_filename
import db_manager
from db_manager import *

app = Flask(__name__)
app.secret_key = b'secret_key'

def get_db():
    db = getattr(g, 'db', None)
    if db is None:
        db = mysql.connector.connect(
            host='farmdb.cprt1kzl5myl.us-east-2.rds.amazonaws.com',
            user='admin',
            password='autoKar_09',
            database='farmdb'
        )
        g.db = db
    return db

@app.route('/')
def index():
    return render_template("index.html")

@app.route('/create/', methods=['GET', 'POST'])
def create():
    if request.method == 'GET':
        return render_template("create.html")
    else:
        animal_id = request.form['animal_id']
        kind = request.form['kind']
        name = request.form['name']
        year, sex = 0, "default"
        status = db_manager.create_animal(animal_id, kind, name, year, sex)
        rows = db_manager.select_all()
        if "success" in status:
            flash(status)
            return redirect(f'/update/{animal_id}')
        else:
            flash(status)
            return redirect('/create/')

@app.route("/list/")
def listing():
    rows = db_manager.select_all()
    return render_template("list.html", animals=rows)

@app.route("/update/<animal_id>", methods=['GET','POST'])
def update(animal_id):
    if request.method == 'GET':
        the_animal = db_manager.select_by_id(animal_id)
        return render_template('create.html', animal=the_animal)
    else:
        kind = request.form['kind']
        name = request.form['name']
        year, sex = 0, "default"
        status = db_manager.update_animal(animal_id, kind, name, year, sex)
        flash(status)
        return redirect(f'/update/{animal_id}')

@app.route('/delete/<animal_id>')
def delete(animal_id):
    status = db_manager.delete_animal(animal_id)
    flash(f"{status}")
    return redirect('/list/')

@app.route("/browse/")
@app.route("/browse/<kind>/<name>/")
def browse(kind=None, name=None):
    if not kind and not name:
        kinds = db_manager.get_kinds()
        kinds = [kind[0] for kind in kinds]
        names = []
        for kind in kinds:
            rows = db_manager.get_names(kind)
            rows = [row[0] for row in rows]
            names.append(rows)
        the_data = {'kinds':kinds, 'names':names}
        
        return render_template("browse.html", data=the_data)
    else:
        rows = db_manager.select({'kind':kind, "name":name})
        
        return render_template("list.html", animals=rows)

@app.route('/search', methods=['GET','POST'])
def search():
    if request.method=="GET":
    
        return render_template("search.html")
    else:
        args = {}
        for key, val in request.form.items():
            if val:
                args[key]=val
        rows = db_manager.select(args)
        
        return render_template("list.html", animals=rows)

@app.route('/import/', methods=['GET','POST'])
def import_data():
    if request.method=='GET':
    
        return render_template("import.html")
        
@app.route('/reset/')
def reset():
    #db_manager.reset_database()
    flash("Database Reset: Success!")
    
    return redirect('/')

#KILL APP:
@app.teardown_appcontext
def close_db(error):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()